import axios from "axios";

const BASE_URL = "https://news.warsono.id";

export default {
  trendingArticles(token, page) {
    return axios.get(`${BASE_URL}/api/v1/posts?sortBy=title${page}`, {
      headers: { Authorization: `Bearer ${token}` }
    });
  },

  latestArticles(token, page) {
    return axios.get(`${BASE_URL}/api/v1/posts?sortBy=id${page}`, {
      headers: { Authorization: `Bearer ${token}` }
    });
  },

  publishArticle(token, article) {
    return axios.post(`${BASE_URL}/api/v1/posts`, article, {
      headers: { Authorization: `Bearer ${token}` }
    });
  }
};
