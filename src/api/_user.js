import axios from "axios";

const BASE_URL = "https://news.warsono.id";

export default {
  register(name, email, password, confirmPassword) {
    return axios.post(`${BASE_URL}/api/v1/auth/register`, {
      name: name,
      email: email,
      password: password,
      password_confirmation: confirmPassword
    });
  },

  login(email, password, rememberMe) {
    return axios.post(`${BASE_URL}/api/v1/auth/login`, {
      email: email,
      password: password,
      remember_me: rememberMe
    });
  },

  getUser(token) {
    return axios.get(`${BASE_URL}/api/v1/auth/me`, {
      headers: { Authorization: `Bearer ${token}` }
    });
  },

  logout(token) {
    return axios.get(`${BASE_URL}/api/v1/auth/logout`, {
      headers: { Authorization: `Bearer ${token}` }
    });
  }
};
