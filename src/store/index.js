import Vue from "vue";
import Vuex from "vuex";
// import axios from "axios";

import articles from "./modules/articles";
import user from "./modules/user";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    articles,
    user
  }
});
