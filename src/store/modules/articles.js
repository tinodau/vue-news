import articles from "../../api/_articles";

const state = {
  noImage:
    "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg",
  trending: [],
  latest: [],
  topSources: [],
  urlArticleContent: "",
  totalPage: "",
  publishedPost: ""
};

const mutations = {
  TRENDING_ARTICLES(state, trending) {
    state.trending = trending;
  },

  LATEST_ARTICLES(state, latest) {
    state.latest = latest;
  },

  TOP_SOURCES(state, sources) {
    state.topSources = sources;
  },

  TOTAL_PAGE(state, page) {
    state.totalPage = page;
  },

  PUBLISHED_POST(state, publishedPost) {
    state.publishedPost = publishedPost;
  }
};

const actions = {
  trendingArticles({ commit }, page) {
    return articles
      .trendingArticles(localStorage.getItem("token"), page)
      .then(res => {
        const articles = res.data.data;
        const totalPage = res.data.meta.last_page;
        let topSources = [];
        for (let article of articles) {
          topSources.push(article.user.name);
        }
        const uniqueTopSources = [...new Set(topSources)];
        commit("TOP_SOURCES", uniqueTopSources);
        commit("TRENDING_ARTICLES", articles);
        commit("TOTAL_PAGE", totalPage);
      })
      .catch(err => console.log(err));
  },

  latestArticles({ commit }, page) {
    return articles
      .latestArticles(localStorage.getItem("token"), page)
      .then(res => {
        const articles = res.data.data;
        const totalPage = res.data.meta.last_page;
        commit("LATEST_ARTICLES", articles);
        commit("TOTAL_PAGE", totalPage);
      })
      .catch(err => console.log(err));
  },

  publishPost({ commit }, article) {
    return articles
      .publishArticle(localStorage.getItem("token"), article)
      .then(res => {
        commit("PUBLISHED_POST", res.data.data);
      })
      .catch(err => console.log(err));
  },

  toKebabCase(title) {
    return title
      .match(
        /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g
      )
      .map(x => x.toLowerCase())
      .join("-");
  }
};

const getters = {
  getTrending: state => {
    return state.trending;
  },

  getLatest: state => {
    return state.latest;
  },

  detailArticle: state => url => {
    return (
      state.trending.find(article => article.slug === url) ||
      state.latest.find(article => article.slug === url)
    );
  },

  getAuthor: state => id => {
    const author = state.latest.find(article => article.user_id == id);
    return author != null ? author.user.name : "User not found!";
  },

  getArticlesByUser: state => id => {
    const articles = state.latest.filter(article => article.user_id == id);
    return articles != null ? articles : "No article!";
  },

  getTotalPage: state => {
    return state.totalPage;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
