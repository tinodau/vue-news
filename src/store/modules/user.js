import _user from "../../api/_user";

const state = {
  token: "",
  registrationStatus: "",
  registrationMessage: "",
  loginStatus: "", // 200 = OK || 401 = Unauthorized
  user: {},
  logoutMessage: "",
  myId: ""
};

const mutations = {
  SET_TOKEN(state, token) {
    state.token = token;
  },

  REGISTRATION_STATUS(state, status) {
    state.registrationStatus = status;
  },

  REGISTRATION_MESSAGE(state, message) {
    state.registrationMessage = message;
  },

  LOGIN_STATUS(state, status) {
    state.loginStatus = status;
  },

  SET_USER(state, user) {
    state.user = user;
  },

  LOGOUT_MESSAGE(state, message) {
    state.logoutMessage = message;
  },

  MY_ID(state, id) {
    state.myId = id;
  }
};

const actions = {
  registerUser({ commit }, { name, email, password, confirmPassword }) {
    return _user
      .register(name, email, password, confirmPassword)
      .then(res => {
        commit("REGISTRATION_MESSAGE", res.data.message);
        commit("REGISTRATION_STATUS", res.status);
      })
      .catch(err => commit("REGISTRATION_STATUS", err.response.status));
  },

  login({ commit }, { email, password, rememberMe }) {
    return _user
      .login(email, password, rememberMe)
      .then(res => {
        const token = res.data.data.access_token;
        localStorage.setItem("token", token);
        commit("SET_TOKEN", localStorage.getItem("token"));
        commit("LOGIN_STATUS", res.status);
      })
      .catch(err => commit("LOGIN_STATUS", err.response.status));
  },

  setToken({ commit }) {
    commit("SET_TOKEN", localStorage.getItem("token"));
    return;
  },

  setUser({ state, commit }) {
    return _user
      .getUser(state.token)
      .then(res => {
        const id = res.data.data.id;
        localStorage.setItem("myId", id);
        commit("MY_ID", id);
        commit("SET_USER", res.data.data);
      })
      .catch(err => commit("LOGIN_STATUS", err.response.status));
  },

  logout({ getters, commit }) {
    return _user
      .logout(getters.getToken)
      .then(res => {
        localStorage.setItem("token", "");
        localStorage.setItem("myId", "");
        commit("SET_TOKEN", null);
        commit("LOGOUT_MESSAGE", res.data.message);
      })
      .catch(err => console.log(err));
  }
};

const getters = {
  getRegistrationMessage: state => {
    return state.registrationMessage;
  },

  getRegistrationStatus: state => {
    return state.registrationStatus;
  },

  getLoginStatus: state => {
    return state.loginStatus;
  },

  getUser: state => {
    return state.user;
  },

  getToken: state => {
    return state.token;
  },

  getId: state => {
    return state.myId;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
