import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Article from "./views/Article.vue";
import Trending from "./views/Trending.vue";
import Latest from "./views/Latest.vue";
import NewsByUser from "./views/NewsByUser.vue";
import Posts from "./views/Posts.vue";
import MyPosts from "./components/MyPosts.vue";
import NewPost from "./components/NewPost.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/trending",
      name: "trending",
      component: Trending
    },

    {
      path: "/latest",
      name: "latest",
      component: Latest
    },
    {
      path: "/posts",
      component: Posts,
      children: [
        {
          path: "/",
          name: "posts",
          component: MyPosts
        },
        {
          path: "/posts/new",
          name: "newPost",
          component: NewPost
        }
      ]
    },
    {
      path: "/:user",
      name: "newsByUser",
      component: NewsByUser
    },
    {
      path: "/:user/:article",
      // path: "/:source/:article",
      name: "article",
      component: Article
    }
  ]
});
